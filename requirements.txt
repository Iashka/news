Django==1.7.10
MySQL-python==1.2.5
argparse==1.2.1
django-annoying==0.8.4
pylibmc==1.5.0
sqlparse==0.1.16
wsgiref==0.1.2
