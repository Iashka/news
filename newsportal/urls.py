from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('newsportal.news.views',
    # Examples:
    url(r'^$', 'index'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^(?P<tid>\d+)/$', 'news_by_tag', name='by_tag'),
    url(r'^admin/', include(admin.site.urls)),
)
