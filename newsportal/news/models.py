# -*- coding: utf-8 -*-

from newcache import cache
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import pre_delete, m2m_changed


class Tags(models.Model):

    name = models.CharField(unique=True, max_length=50)

    def __init__(self, *args, **kwargs):
        super(Tags, self).__init__(*args, **kwargs)
        self.cache_key = 'tag{id}'.format(id=self.id)

    def get_last_news(self):
        value = cache.get(self.cache_key)
        if not value:
            value = self.get_cache_value()
            cache.set(self.cache_key, value, 300)
        return value

    def get_cache_value(self):
        return list(self.news.order_by('-date_inserted')[:10])


class News(models.Model):

    title = models.CharField(max_length=255, null=False)
    announce = models.CharField(max_length=1000, default='')
    text = models.CharField(max_length=20000, null=False)
    date_inserted = models.DateTimeField(auto_now_add=True, db_index=True)
    date_updated = models.DateTimeField(auto_now=True)
    author = models.ForeignKey('auth.User')
    watched = models.IntegerField(default=0)
    tags = models.ManyToManyField(Tags, related_name='news')


@receiver(pre_delete, sender=Tags)
def on_tag_delete(instance, **kwargs):
    cache.delete(instance.cache_key)

@receiver(m2m_changed, sender=News.tags.through)
def on_news_tags_change(instance, reverse, **kwargs):
    # здесь можно было бы обрабатывать только добавление связок,
    # если бы было известно, что новости добавляются значительно чаще,
    # чем удаляются связи тег-новость
    if reverse:
        cache.set(instance.cache_key, instance.get_cache_value())
    else:
        data = {t.cache_key: t.get_cache_value() for t in instance.tags.all()}
        cache.set_many(data, 300)
