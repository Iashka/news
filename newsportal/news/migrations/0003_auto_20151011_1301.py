# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20151011_1157'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='news',
            name='date_updated',
        ),
        migrations.AddField(
            model_name='news',
            name='announce',
            field=models.CharField(default=b'', max_length=1000),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='news',
            name='shown',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='news',
            name='tag',
            field=models.ManyToManyField(related_name='news', to='news.Tags'),
            preserve_default=True,
        ),
    ]
