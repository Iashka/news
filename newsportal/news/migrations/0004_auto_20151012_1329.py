# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0003_auto_20151011_1301'),
    ]

    operations = [
        migrations.RenameField(
            model_name='news',
            old_name='tag',
            new_name='tags',
        ),
        migrations.RenameField(
            model_name='news',
            old_name='shown',
            new_name='watched',
        ),
        migrations.AddField(
            model_name='news',
            name='date_updated',
            field=models.DateTimeField(default=1, auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='news',
            name='date_inserted',
            field=models.DateTimeField(auto_now_add=True, db_index=True),
            preserve_default=True,
        ),
    ]
