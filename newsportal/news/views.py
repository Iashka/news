from annoying.decorators import render_to
from models import Tags

@render_to('index.tpl')
def index(request, **kwargs):
    context = {'tags': Tags.objects.values()}
    return context

@render_to('detail.tpl')
def news_by_tag(request, **kwargs):
    context = {'news': Tags.objects.get(pk=kwargs['tid']).get_last_news()}
    return context

