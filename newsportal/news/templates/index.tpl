{% extends 'base.tpl' %}
{% block title %}T A G S{% endblock %}
{% block page %}
<div>
    <table width="100%" border="5 px">
        <tr>
            {% for i in tags %}
                <td width="20%" align="center">
                    <h3>
                        <a href="{% url 'by_tag' tid=i.id %}">{{ i.name }}</a>
                    </h3>
                </td>
                {% if forloop.counter|divisibleby:'5' %}</tr><tr>{% endif %}
            {% endfor %}
        </tr>
    </table>
</div>
{% endblock %}