<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>N E W S</title>
    <style>
      body {
        padding: 60px;
      }
    </style>
  </head>

  <body>
    <div>
      <a href="/">T A G S</a>
    </div>
    <div>
      <center><h1>{% block title %}{% endblock %}</h1></center>
      {% block page %}{% endblock %}
    </div>
  </body>
</html>
