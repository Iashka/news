{% extends 'base.tpl' %}
{% block title %}N E W S{% endblock %}
{% block page %}
<div>
    {% for i in news %}
        <h2>{{ i.title }}</h2>
        <h3>{{ i.announce }}</h3><span>{{ i.date_inserted|date:"Y-m-d H:i:s" }}</span>
        <hr/>
    {% endfor %}
</div>
{% endblock %}