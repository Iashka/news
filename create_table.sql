CREATE TABLE `news_news` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`text` VARCHAR(20000) NOT NULL COLLATE 'utf8_unicode_ci',
	`date_inserted` DATETIME NOT NULL,
	`date_updated` DATETIME NOT NULL,
	`watched` INT(11) NOT NULL,
	`author_id` INT(11) NOT NULL,
	`announce` VARCHAR(1000) NOT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`),
	INDEX `news_news_author` (`author_id`),
	INDEX `news_news_date` (`date_inserted`)
)


CREATE TABLE `news_tags` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `name` (`name`)
)


CREATE TABLE `news_news_tags` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`news_id` INT(11) NOT NULL,
	`tags_id` INT(11) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `news_id` (`news_id`, `tags_id`),
	INDEX `news_news_tag_c660c5da` (`news_id`),
	INDEX `news_news_tag_8f9ab750` (`tags_id`)
)
